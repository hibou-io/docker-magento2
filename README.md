# docker-magento2

A base Magento 2 image that can be used to scale in production. This can
be used in combination with MySQL and Redis. It is opinionated and includes
support for Composer, ionCube, Redis, OPcache, S3FS and the required PHP
modules for a basic Magento installation.

It does not include Magento.

# Usage

An example `Dockerfile`

```
FROM registry.gitlab.com/hibou-io/docker-magento2:7.2
COPY src/ /var/www/html/
```

More details can be found in [Magento2 and Docker](DOCKER.md).

# Persistent storage

The container assumes you do not store data in a folder along with the
application. Don't use Docker volumes for scale. Use CephFS, GlusterFS or
integrate with S3 (or MinIO!) via the built in S3FS.

# Configuration

## Environment variables

Environment variable  | Description                   | Default
--------------------  | -----------                   | -------
MYSQL_HOSTNAME        | MySQL hostname                | mysql
MYSQL_USERNAME        | MySQL username                | root
MYSQL_PASSWORD        | MySQL password                | secure
MYSQL_DATABASE        | MySQL database                | magento
CRYPTO_KEY            | Magento Encryption key        | Emtpy
URI                   | Uri (e.g. http://localhost)   | http://localhost
RUNTYPE               | Set to development to enable  | Empty
ADMIN_USERNAME        | Administrator username        | admin
ADMIN_PASSWORD        | Administrator password        | adm1nistrator
ADMIN_FIRSTNAME       | Administrator first name      | admin
ADMIN_LASTNAME        | Administrator last name       | admin
ADMIN_EMAIL           | Administrator email address   | admin@localhost.com
BACKEND_FRONTNAME     | The URI of the admin panel    | admin
CURRENCY              | Magento's default currency    | EUR
LANGUAGE              | Magento's default language    | en_US
CONTENT_LANGUAGES     | The languages used in Magento | en_US
TIMEZONE              | Magento's timezone            | Europe/Amsterdam
CRON                  | Run as a cron container       | false
UNATTENDED            | Run unattended upgrades       | false
FORCE_UPGRADE         | Set to "true" to force setup:upgrade | 
S3FS_BUCKET           | Bucket e.g. my-bucket         |
S3FS_ACCESS_KEY       | Access Key e.g. test          |
S3FS_SECRET_KEY       | Secret Key e.g. 987654321abc  |
S3FS_MNT_POINT        | Mount Point*1 e.g. /var/www/html/pub/media |
S3FS_MNT_OPTIONS      | Mount Options*2 e.g. url=http://minio:9000,allow_other,uid=33,gid=33,use_cache=/tmp,max_stat_cache_size=1000,stat_cache_expire=900,retries=5,connect_timeout=10,use_path_request_style |

*1 You may want to initially mount it to something like `/mnt/bucket` to allow you to verify connection and then copy your data into it.
*2 These options work with minio, you can remove the url and use_path_request_style if you're in actual AWS S3

Include the port mapping in `URI` if you run your shop on a local development
environment, e.g. `http://localhost:3000/`.

## Hooks

Hooks allow you to run certain actions at certain points in the startup
procedure without having to rewrite `entrypoint.sh`. You can use hooks to
import data, preconfigure settings or run other scripts that need to run
each startup sequence.

Hook point   | Location                 | Description
----------   | ---------                | -----------
Pre install  | `/hooks/pre_install.sh`  | Runs before an installation or update.
Pre compile  | `/hooks/pre_compile.sh`  | Runs before code compilation starts.
Post install | `/hooks/post_install.sh` | Runs after Magento has been installed.

You need to `COPY` any hooks to this location yourself.

## Development mode

Setting `RUNTYPE` to `development` will turn on public error reports. Anything
else will leave it off. It will also set `display_errors` to on in PHP. This is
set to off by default.

A basic `docker-compose.yml` has been provided to help during the development
of this image.
